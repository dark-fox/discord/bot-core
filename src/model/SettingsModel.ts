//Base
import { getRepository, Repository } from 'typeorm';

//Configs
import config from '../../config/config.json';

//Entities
import { Settings } from '../entities/Settings';

//Interfaces
import { IResponse } from './interfaces/IResponse';

//Model
import { Model } from './Model';

export class SettingsModel extends Model {
  private repository: Repository<Settings>;

  constructor() {
    super();
    this.repository = getRepository(Settings);
  }

  /**
   * Replace current prefix with given one.
   *
   * @param {string} prefix    - New prefix to set.
   * @param {number} [guildId] - Id of guild stored in database.
   */
  public async setPrefix(prefix: string, guildId: number = this.guildId): Promise<IResponse> {
    if (0 >= prefix.length) {
      return this.failResponse('You can\`t add an empty prefix.');
    }

    const Settings: Settings|undefined = await this.getSettings(guildId);

    if (Settings && Settings.prefix !== prefix) {
      Settings.prefix = prefix;
      return await this.repository.save(Settings)
        .then(() => this.successResponse(`New prefix set to \`${prefix}\`.`))
        .catch(error => {
          console.error('[ERROR] SettingsModel.setPrefix() error');
          console.error(error);

          return this.failResponse(`Can't store \`${prefix}\` as prefix for guild ${guildId} in database.`);
      });
    }

    return this.failResponse(`There was problem while bot was trying to change guild ${guildId} prefix to \`${prefix}\`.`);
  }

  /**
   * Set new role as permitted one.
   *
   * @param {string} role      - New role to add.
   * @param {number} [guildId] - Id of guild stored in database.
   */
  public async addRole(role: string, guildId: number = this.guildId): Promise<IResponse> {
    if (0 >= role.length) {
      return this.failResponse('You can\`t add an empty role.');
    }

    const Settings: Settings|undefined = await this.getSettings(guildId);

    if (Settings) {
      if (!Settings.permittedRoles) {
        Settings.permittedRoles = [];
      }

      if (-1 === Settings.permittedRoles.indexOf(role)) {
        Settings.permittedRoles.push(role);

        return await this.repository.save(Settings)
          .then(() => this.successResponse(`Permissions for role \`${role}\` created.`))
          .catch(error => {
            console.error('[ERROR] SettingsModel.addRole() error');
            console.error(error);

            return this.failResponse(`Can't store role ${role} in database.`);
          });
      } else {
        return this.failResponse(`Role ${role} is already set on permitted roles list.`);
      }
    }

    return this.failResponse('There was problem while bot was trying to add role.');
  }

  /**
   * Remove role from permitted list.
   *
   * @param {string} role      - Role to remove.
   * @param {number} [guildId] - Id of guild stored in database.
   */
  public async removeRole(role: string, guildId: number = this.guildId): Promise<IResponse> {
    if (0 >= role.length) {
      return this.failResponse('You can\`t remove an empty role.');
    }

    const Settings: Settings|undefined = await this.getSettings(guildId);

    if (Settings) {
      const rolePosition: number = Settings.permittedRoles.indexOf(role);

      if (0 <= rolePosition) {
        Settings.permittedRoles.splice(rolePosition, 1);

        return await this.repository.save(Settings)
          .then(() => this.successResponse(`Permissions for role \`${role}\` removed.`))
          .catch(error => {
            console.error('[ERROR] SettingsModel.removeRole() error');
            console.error(error);

            return this.failResponse(`Can't remove role ${role} from database.`);
          });
      } else {
        return this.failResponse(`Role ${role} isn't set on permitted roles list.`);
      }
    }

    return this.failResponse('There was problem while bot was trying to remove role.');
  }

  /**
   * Set new value of permitted roles for given guild.
   *
   * @param {string[]} roles     - New roles to set.
   * @param {number}   [guildId] - Id of guild stored in database.
   */
  public async setRoles(roles: string[], guildId: number = this.guildId): Promise<IResponse> {
    if (0 >= roles.length) {
      return this.failResponse('You can\`t replace current roles with empty data.');
    }

    const Settings: Settings|undefined = await this.getSettings(guildId);

    if (Settings) {
      Settings.permittedRoles = roles;
      return await this.repository.save(Settings)
        .then(() => this.successResponse('Roles successfully set.'))
        .catch(error => {
          console.error('[ERROR] SettingsModel.setRoles() error');
          console.error(error);

          return this.failResponse('Can\'t set new roles list in database.');
        });
    }

    return this.failResponse('There was problem while bot was trying to set roles list.');
  }

  /**
   * Create new entry in GuildSettings table.
   *
   * @param {number} [guildId] - Id of guild to be stored in database.
   */
  public async createGuildEntry(guildId: number = this.guildId): Promise<IResponse> {
    if (! await this.getSettings(guildId)) {
      const SettingsEntity: Settings = new Settings;
      SettingsEntity.guild = guildId;
      SettingsEntity.prefix = config.prefix;

      await SettingsEntity.save()
        .then(() => this.successResponse(`Settings entry for guild ${guildId} successfully created.`))
        .catch(error => {
          console.error('[ERROR] SettingsModel.createGuildEntry() error');
          console.error(error);

          return this.failResponse(`Can't create settings entry for guild ${guildId}.`);
        });
    }

    return this.failResponse(`There was problem while bot was trying create settings entry for guild ${guildId}.`);
  }

  /**
   * Remove command from disabled list.
   *
   * @param {string} command - Command name.
   * @param {number} guildId - Id of guild to be stored in database.
   */
  public async enableCommand(command: string, guildId: number = this.guildId): Promise<IResponse> {
    if (0 >= command.length) {
      return this.failResponse('Command to remove from disabled list is not set.');
    }

    const Settings: Settings|undefined = await this.getSettings(guildId);

    if (Settings) {
      const commandPosition: number = Settings.disabledCommands.indexOf(command);

      if (0 <= commandPosition) {
        Settings.disabledCommands.splice(commandPosition, 1);

        return await this.repository.save(Settings)
          .then(() => this.successResponse(`Command \`${command}\` removed from disabled list.`))
          .catch(error => {
            console.error('[ERROR] SettingsModel.enableCommand() error');
            console.error(error);

            return this.failResponse(`Can't remove command \`${command}\` from disabled list.`);
          });
      } else {
        return this.failResponse(`Command \`${command}\` isn't set on disabled commands list.`);
      }
    }

    return this.failResponse('There was problem while bot was trying to remove command from disabled commands list.');
  }

  /**
   * Add command to disabled list.
   *
   * @param {string} command - Command name.
   * @param {number} guildId - Id of guild to be stored in database.
   */
  public async disableCommand(command: string, guildId: number = this.guildId): Promise<IResponse> {
    if (0 >= command.length) {
      return this.failResponse('Command to add to disabled list is not set.');
    }

    const Settings: Settings|undefined = await this.getSettings(guildId);

    if (Settings) {
      if (!Settings.disabledCommands) {
        Settings.disabledCommands = [];
      }

      if (-1 === Settings.disabledCommands.indexOf(command)) {
        Settings.disabledCommands.push(command);

        return await this.repository.save(Settings)
          .then(() => this.successResponse(`Command \`${command}\` is now set as disabled.`))
          .catch(error => {
            console.error('[ERROR] SettingsModel.addRole() error');
            console.error(error);

            return this.failResponse(`Can't store command \`${command}\` in disabled commands list.`);
          });
      } else {
        return this.failResponse(`Command \`${command}\` is already disabled.`);
      }
    }

    return this.failResponse('There was problem while bot was trying to add command to disabled commands list.');
  }

  /**
   * Set new channel if to display reports.
   *
   * @param {string|null} channel   - Channel id or null, if functionality should be disabled.
   * @param {number}      [guildId] - Id of guild stored in database.
   */
  public async setReportChannel(channel: string|null, guildId: number = this.guildId): Promise<IResponse> {
    const Settings: Settings|undefined = await this.getSettings(guildId);

    if (Settings) {
      //@ts-ignore
      Settings.reportChannel = channel;

      return await this.repository.save(Settings)
        .then(() => this.successResponse(`Reports channel set to \`${channel || 'off'}\`.`))
        .catch(error => {
          console.error('[ERROR] SettingsModel.setReportChannel() error');
          console.error(error);

          return this.failResponse(`Can't set reports channel to \`${channel || 'off'}\` for guild ${guildId} in database.`);
        });
    }

    return this.failResponse(`There was problem while bot was trying to set reports channel for guild ${guildId}. Channel: "${channel || 'off'}".`);
  }

  /**
   * Return all Settings for Guild.
   *
   * @param {number} [guildId] - Database id where Guild is stored.
   */
  public async getSettings(guildId: number = this.guildId): Promise<Settings|undefined> {
    return await this.repository.findOne({ where: { guild: guildId } });
  }

}