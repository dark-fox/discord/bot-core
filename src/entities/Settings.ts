import { BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { Guilds } from './Guilds';

@Entity()
@Unique(['guild'])
export class Settings extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @OneToOne(type => Guilds)
  @JoinColumn()
  guild!: number;

  @Column({ nullable: false, default: 'h!' })
  prefix!: string

  @Column({ name: 'prefix_case_sensitive', nullable: false, default: false })
  prefixCaseSensitive!: boolean;

  @Column({ name: 'permitted_roles', type: 'jsonb', nullable: true, default: null })
  permittedRoles!: string[];

  @Column({ name: 'disabled_commands', type: 'jsonb', nullable: true, default: null })
  disabledCommands!: string[];

  @Column({ name: 'report_channel', nullable: true, default: null })
  reportChannel!: string;

}