//Core
import { BotClient } from './BotClient';

export class QueueManager {
  protected Client!: BotClient;

  public run(): this {
    // Run queues here if needed.
    return this;
  }

  public setClient(Client: BotClient): QueueManager {
    this.Client = Client;
    return this;
  }

}