import { Connection, createConnection } from 'typeorm';

export class DbSync {
  public async sync(): Promise<void> {
    const db: Connection = await createConnection();

    return await db.synchronize();
  }
}

try {
  (new DbSync).sync()
    .then(async () => {
      console.log('> db synchronized');
    }).catch(exception => console.error(exception)); // @ts-ignore
} catch (error) {
  console.error(error);
}
