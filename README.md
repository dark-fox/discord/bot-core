# Bot Core base

## Index
1. [Requirements](#Requirements)
1. [Before run](#before-run)
    1. [config.json](#config.json)
        1. [file overview](#file-overview)
    1. [ormconfig.json](#ormconfig.json)
1. [How to run?](#how-to-run)
1. [Logic behind application](#logic-behind-application)
1. [How to add command?](#how-to-add-command)
    1. [Create new command type](#create-new-command-type)
    1. [Extend command type](#extend-command-type)
1. [Database](#database)

## Requirements
- NodeJS v. 13.9.0
- PostgreSql 

## Before run
Before run, you have to create two files from their templates.

### config.json
First one is `config/config.json`. Copy content from `config/config.template.json` and fill it with all necessary data. The most important thing to fill is token. You can get one from Discord Developer page. Filling prefix is not mandatory because it can be changed by command.
```json
{
  "prefix": ".",
  "token": "",
}
```

#### file overview
- **base** - prefix used by the bot. It can be changed using command.
- **token** - bot token created on Discord Developer page.
- **rest** - commands used by REST Api
    - **foxRandomPicture** - source of random picture of foxes
- **permissions** - string[], an array that contains roles who have permission to do some things in bot.

### ormconfig.json
Second config file is called `ormconfig.json`. This one contains all data needed by TypeORM - a lib used for connection to project database. This file and its template are located in main project directory. Act the same way as with `config.json` - copy template file content and fill whatever you need.

## How to run?

### First run
First, you should install all dependencies using NPM.
```bash
$ npm install
```
When it's done, you can run application, by typing:
```bash
$ npm run tsc
$ node ./dist/run.js
```
`tsc` will compile TypeScript to JavaScript, next command will run app.

To simplify the whole process, you can just type:
```bash
$ npm run app
```
This will run `tsc` and then run application.

When everything is fine, you should see:
```
> discord.js client logged successfully
> discord.js client ready.
```

## Logic behind application
Application has 2 main directories:
- src,
- config.

In `app` you can find all the project files. Directory `config` is just a place for JSON files.

The very base file is `/src/run.ts` - that's main project file which creates all the logic inside bot.

It listens to `Discord.Client()` and when events and do all necessary actions to make bot fully functional.

Another important file is `core/Foxy.ts`. Now this file doesn't do much more than runnig `MessageProcessor` - a class that process messages typed by user and depends on received data run commands.

## How to add command?
At first, edit `config/commands.json` file. Add new part which looks like this:
```json
"COMMAND_NAME": {
  "type": "COMMAND_TYPE",
  "help": "OPTIONAL HELP"
}
```

The most important part here is `type`. It determines what bot should run to process command. We have here 2 cases:
1. We want to create new type.
2. We want to extend type, for example `settings`.

They're similar, but we should treat them separately. Be sure to read both tutorials to fully understand how it works.

### Create new command type
In this case, we want to create new type called "music".

1. Add your command to `commands.json`.
1. Create new file in `/src/commands`. File should be named with `CamelCase`. Name should be the same as type you created, so it should be `Music.ts`.
1. Open that file and create new class `Music` - name of class should be the same as file name. Class also should be extended by class `Command`.
1. Add asynchronous method called `process`, thanks to that it'll be run automatically.
1. Open `/src/core/MessageProcessor.ts` and find method `setMessageFactory`. There is switch where you have to create new case:
    ```
    case 'music':
      this.Command = new Music;
      break;
    ```
   Note that `case` must be same as type you put in `commands.json`. Next you want to set for `this.Command` property instance of class created in point 2.
1. That's all. Everything else depends on your `Music` class. When user type command that have type `music` it'll run method `process` in your class `Music`. 

### Extend command type
In this case we want to create command `joke` with type `fun`.

1. Add your command to `commands.json`.
1. Go to `/src/commands` and open file `Fun.ts`.
1. Find method `process`. There should `swich`.
1. Add new case called `joke`. Like in case `fox`, you need to return some method that returns instance of current class. 
    ```ts
    case 'joke':
      return this.processJoke();
    ```
   Name of your method could be whatever you want.
1. Create your method.
    ```ts
    protected async processJoke(): Promise<this> {
      await this.sendMessage('This is a joke. Ha Ha.');
      return this;
    } 
   ```
1. That's all. When someone will use your command your method is going to be run and excellent joke will be sent.

## Database
Bot needs database to be functional. It's prepared to work with PostgreSql through TypeORM library. It should work with any other database supported by Keyv, but it wasn't tested.